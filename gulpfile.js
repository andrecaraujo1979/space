var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var cssnano      = require('cssnano');
var browserSync = require('browser-sync').create();

var PATH = {
	css: 'style/css/',
	scss: 'style/scss/'
};

gulp.task('sass', function(){
    return gulp.src(PATH.scss + 'style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(PATH.css))
    .pipe(browserSync.stream())
 });

gulp.task('styles', function(){
    var processors = [
        autoprefixer(),
        cssnano(),
     ];
     return gulp.src(PATH.css + 'style.css')
        .pipe(sourcemaps.init())
        .pipe(postcss(processors))
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest(PATH.css))
        .pipe(browserSync.stream());
});

gulp.task('watch', function(){

    browserSync.init({
        proxy:"localhost/space"
    });
 
    gulp.watch(PATH.scss + '*.scss', gulp.series(['sass', 'styles']));
    gulp.watch(PATH.css + '*.css').on('change', browserSync.stream);
 });
 
 gulp.task('default', gulp.series(['sass', 'watch', 'styles']));